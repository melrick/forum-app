# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.http import HttpResponse
from django.shortcuts import render
from  .models import *

# Create your views here.

def index(request):
    return HttpResponse("Hello world")

def question(request):
    question_list = Users.objects.order_by('id')
    context = {'latest_list': question_list }
    return render(request,'publicForum/index.html',context)
