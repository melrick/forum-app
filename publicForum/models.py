# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

class Users(models.Model):
    name = models.CharField(max_length = 100)
    email = models.EmailField()
    password = models.CharField(max_length=100)

class Questions(models.Model):
    question_text = models.CharField(max_length = 100)
    uid = models.ForeignKey(Users)

class Comments(models.Model):
    comment_text = models.CharField(max_length=100)
    qid = models.ForeignKey(Questions)
    cid = models.ForeignKey("Comments",default=0)
